# herosphp/validate

在中间件上进行验证


### install
```shell
composer install herosphp/validate

```

### usage
在中间件层验证参数是正确
```php
    <?php

namespace app\middleware;

use herosphp\core\HttpRequest;
use herosphp\core\MiddlewareInterface;
use herosphp\plugin\validate\Valid;
use herosphp\plugin\validate\Validate;
use herosphp\plugin\validate\ValidateException;
use herosphp\WebApp;

class ValidateMiddleware implements MiddlewareInterface
{
    /**
     * @throws \ReflectionException
     */
    public function process(HttpRequest $request, callable $handler)
    {
        $reflectionMethod = new \ReflectionMethod(WebApp::$_targetController, WebApp::$_targetMethod);
        $reflectionAttributes = $reflectionMethod->getAttributes(Valid::class);
        if ($reflectionAttributes) {
            foreach ($reflectionAttributes as $validAttribute) {
                /** @var Valid $methodValidInstance */
                $methodValidInstance = $validAttribute->newInstance();
                $methodVInstance = new ($methodValidInstance->class);
                if (!$methodVInstance instanceof Validate) {
                    throw new ValidateException("{$methodVInstance->class} must extend \\herosphp\\plugin\\validate\Validate");
                }
                $methodVInstance->scene($methodValidInstance->scene)->check([...$request->get(), ...$request->post()]);
            }
        }
        return $handler($request);
    }
}

```
