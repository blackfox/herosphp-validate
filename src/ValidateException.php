<?php
declare(strict_types=1);
namespace herosphp\plugin\validate;

/**
 * validate exception
 */
class ValidateException extends \RuntimeException
{

}
